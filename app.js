var express = require('express');
var gaikan = require('gaikan');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();

var routes = require('./routes/html');
var apiroute = require('./routes/api');
var models = require('./models');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', gaikan);
app.set('view engine', '.html');
console.log('hola');
// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/moment', express.static(path.join(__dirname, '/node_modules/moment/min/')));

// ORM2 setting conection and models
app.use(function(req, res, next) {
    models(function(err, db) {
        if (err) return next(err);
        req.models = db.models;
        req.db = db;
        return next();
    });
});

app.use('/', routes);
app.use('/api', apiroute);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// development error handler
// // will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         console.error('Error Message: <br>' + err.message + '');
//     });
// }

// // production error handler
// // no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     console.error('Error Message: <br>' + err.message + '');
// });


module.exports = app;
