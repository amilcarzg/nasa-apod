var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res, next) {
	function normalizePort(val) {
	  var port = parseInt(val, 10);

	  if (isNaN(port)) {
	    // named pipe
	    return val;
	  }

	  if (port >= 0) {
	    // port number
	    return port;
	  }

	  return false;
	}
 	var port = normalizePort(process.env.PORT || '3000');
  	res.render('index', { title: 'NASA - APOD', subtitle: 'Search Date', puerto: port  });
});

router.get('/search', function(req, res, next) {
	console.log('render');
  res.render('search', { title: 'Search template' });
});

router.get('/history', function(req, res, next) {
  res.render('history', { title: 'History template' });
});

module.exports = router;
