var express = require('express');
var router = express.Router();
var _ = require('lodash');

/*Routes apods*/
router.post('/apod', function(req, res, next) {
    var params = _.pick(req.body, 'date', 'name');
    req.models.apods.create(params, function(err, items) {
        (err !== null) ? res.status(500).json({ resp: 'fail' }): res.json(_.assignIn({ resp: 'ok' }, _.pick(items, 'id')));
    });
});

router.get('/apod', function(req, res, next) {
    req.models.apods.find().run(function(err, apods) {
        (err !== null) ? res.status(500).json({ resp: 'fail' }): res.json(apods);
    });
});

router.get('/delapod/:id', function(req, res, next) {
    req.models.apods.get(req.params.id, function(error, apodSelect) {
        if (error !== undefined) {
            res.status(500).json({ resp: 'apod not found' });
        } else {
            apodSelect.remove(function(e) {
                (e !== null) ? res.status(500).json({ resp: 'fail removing' }): res.json({ resp: 'ok' });
            });
        }
    });
});

/*Routes apod_items*/

router.post('/apoditem', function(req, res, next) {
    var params = _.assignIn({ apod_id: req.body.parent }, _.pick(req.body, 'date', 'explanation', 'media_type', 'title', 'url'));
    req.models.apod_items.create(params, function(err, items) {
        console.log(err);
        (err !== null) ? res.status(500).json({ resp: 'fail' }): res.json(_.assignIn({ resp: 'ok' }, _.pick(items, 'id')));
    });
});

router.get('/apoditem/:id', function(req, res, next) {
    var param = { apod_id: req.params.id };
    req.models.apod_items.find(param).run(function(err, apod_items) {
        (err !== null) ? res.status(500).json({ resp: 'fail' }): res.json(apod_items);
    });
});

router.get('/delapoditem/:id', function(req, res, next) {
    var param = { apod_id: req.params.id };
    req.models.apod_items.find(param).remove(function(e) {
        (e !== null) ? res.status(500).json({ resp: 'fail removing' }): res.json({ resp: 'ok' });
    });

});

module.exports = router;
