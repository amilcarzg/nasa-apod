var orm = require('orm');
var MigrateTask = require('migrate-orm2');

var settings = require('./settings');

var connection = null;

function setup(db, cb) {
    require('./entity/apods')(orm, db);
    require('./entity/apod_items')(orm, db);
    db.models.apod_items.hasOne('apod', db.models.apods, { required: true, reverse:'items' });
    return cb(null, db);
}

module.exports = function(cb) {
    if (connection) return cb(null, connection);

    orm.connect(settings.path, function(err, db) {
        if (err)
             throw err;
        connection = db;
        db.settings.set('instance.returnAllErrors', true);
        setup(db, cb);
        db.sync();
    });
};
