module.exports = function (orm, db) {
	db.define('apod_items',{
		date : {type : 'text'},
		explanation : {type : 'text', size:'2000'},
		media_type : {type : 'text', size: '500'},
		title: {type : 'text'},
		url: {type : 'text'}
	});
}