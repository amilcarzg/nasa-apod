'use strict';

/* Controllers */

angular.module('myapp.controllers', []).
controller('historyController', ['$scope', '$http', function($scope, $http) {
    $scope.port= document.getElementById('port').value;
    $scope.nameBefore="";
    $scope.apod=[];
    $scope.apodChilds=[];
     $http.get('http://localhost:'+$scope.port+'/api/apod', {cache: false})
        .then(function(response) {
             $scope.apod=response.data;
        }, function(response) {
            sweetAlert("Oops...", "Fail loading history", "error");
            scope.apod=[];
        });
    $scope.orderStatus=true;
    $scope.deleteApod = function (id,pos,e) {
        e.preventDefault();
        $scope.apodChilds=[];

       $http.get('http://localhost:'+$scope.port+'/api/delapod/'+id, {cache: false})
        .then(function(response) {
                var li=e.target.parentNode.parentNode;
                angular.element(li).empty();
                angular.element(li).remove();
                if($scope.nameBefore=="hspan"+id)
                    $scope.nameBefore=""
                $scope.apod.splice(pos,1);
                $http.get('http://localhost:'+$scope.port+'/api/delapoditem/'+id, {cache: false})
            }, function(response) {
            sweetAlert("Oops...", response.resp, "error");
        });
    }

    $scope.searchChilds = function (id){
        var idclass="hspan"+id;
        $scope.apodChilds=[];
        if(idclass!=$scope.nameBefore){
        if($scope.nameBefore!="")
        document.getElementById($scope.nameBefore).parentNode.className='list-group-item history-text';
            $scope.nameBefore=idclass;
        }
        document.getElementById("hspan"+id).parentNode.className+=' active';  //list-group-item history-text

        $http.get('http://localhost:'+$scope.port+'/api/apoditem/'+id, {cache: false})
        .then(function(response) {
           $scope.apodChilds=response.data;
        }, function(response) {
            sweetAlert("Oops...", "Fail loading history search.", "error");
        });
    }

    $scope.orderHistoy = function() {
        angular.element(document.getElementById("iconOrder")).removeClass() ;
        if ($scope.orderStatus){
            $scope.apod=$scope.apod.sort(function(a, b){ return parseInt(a.date.replace(/[-: ]/g, ""))-parseInt(b.date.replace(/[-: ]/g, ""))});
            document.getElementById("iconOrder").className='glyphicon glyphicon-chevron-up pull-right';
        } else {
            $scope.apod=$scope.apod.sort(function(a, b){ return parseInt(b.date.replace(/[-: ]/g, ""))-parseInt(a.date.replace(/[-: ]/g, ""))});
                        document.getElementById("iconOrder").className='glyphicon glyphicon-chevron-down pull-right';
        }

        $scope.orderStatus=!$scope.orderStatus;
    }

}]).
controller('navController', function($scope) {
    $scope.activeMenu = 1;
    $scope.setActive = function(val) {
        $scope.activeMenu = val;
    }
}).
controller('searchController', ['$scope', '$http', function($scope, $http) {
    var today = new Date();
    var yesterday = new Date(today.setDate(today.getDate() - 1))
    $scope.format = 'yyyy-MM-dd';
    $scope.date = yesterday;
    $scope.datapush = [];
    $scope.searchCount = 0;
    $scope.name="";
    $scope.parentId=0;
    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: yesterday,
        startingDay: 1,
        initDate: yesterday
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    /*Search and draw search api*/
    $scope.drawSearch = function(e) {
        e.preventDefault();
        $http.get('https://api.nasa.gov/planetary/apod', {
            params: {"api_key": "cxNMqsZD13spM03vM6hnOBp82LAF4wRGzm5PdvT9", "date": $scope.date.toISOString().slice(0, 10) },
             cache: false
        }).then(function(response) {
          $scope.datapush.unshift(response.data);
          $scope.searchCount=$scope.searchCount+1;
          $scope.saveSearch(response.data);
        }, function(response) {
            var msg = (response.data!==null)?response.data.msg : " a problem has ocurred.";
            sweetAlert("NASA API error ", msg+'... Try another Date', "error");
        });
    }

    $scope.saveSearch = function(r) {
        var port= document.getElementById('port').value;

        if($scope.name==""){
            var savedate= moment().format('YYYY-MM-DD HH:MM:SS');
            $scope.name='Search '+savedate ;
           $http({
                method : 'POST',
                url : 'http://localhost:'+port+'/api/apod',
                data : {date:savedate,name:$scope.name},
                cache: false,
            }).then(function(response) {
                    $scope.parentId= response.data.id;
                    $http({
                        method : 'POST',
                        url : 'http://localhost:'+port+'/api/apoditem',
                        data : {date:r.date,explanation:r.explanation,media_type:r.media_type,title:r.title,url:r.url,parent:response.data.id},
                        cache: false,
                    }).then(function(response) {
                    }, function(response) {
                        console.log('No se pudo guardar la busqueda');
                    });
             }, function(response) {
                 console.log('No se pudo guardar la busqueda');
             });
        }
        else {
            $http({
                    method : 'POST',
                    url : 'http://localhost:'+port+'/api/apoditem',
                    data : {date:r.date,explanation:r.explanation,media_type:r.media_type,title:r.title,url:r.url,parent:$scope.parentId},
                    cache: false,
                }).then(function(response) {
                    console.log("se guardo exitosamente");
                }, function(response) {
                     console.log('No se pudo guardar la busqueda');
                });
        }
    }

    $scope.clearSearch = function() {
          $scope.parentId=0;
          $scope.datapush=[];
          $scope.name="";
          $scope.searchCount=0;
          angular.element(document.getElementById(timelineContent)).remove();
    }

}]);
