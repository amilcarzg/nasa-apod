'use strict';

var app = angular.module('myapp', ['ngRoute', 'ui.bootstrap', 'myapp.controllers'])
    .config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider.
        when('/search', {
            templateUrl: '/search'
        }).
        when('/history', {
            templateUrl: '/history'
        });
    }]);

app.run(function() {
    location.href = "#search";
})
